ARG PARENT_IMAGE
FROM $PARENT_IMAGE

ARG CONFLUENT_KAFKA_VERSION
ENV CONFLUENT_KAFKA_VERSION=$CONFLUENT_KAFKA_VERSION

ARG SCALA_VERSION
ENV SCALA_VERSION=$SCALA_VERSION

LABEL maintainer="gitlab@therack.io"
LABEL description="Confluent Kafka ${CONFLUENT_KAFKA_VERSION} (Scala: ${SCALA_VERSION})"
LABEL CONFLUENT_KAFKA_VERSION="${CONFLUENT_KAFKA_VERSION}"
LABEL SCALA_VERSION="${SCALA_VERSION}"

RUN \
    apt update && \
    apt -qq -y install wget && \
    apt -qq -y -o Dpkg::Options::="--force-confold" full-upgrade && \
    cd /opt && \
    wget https://s3.wasabisys.com/therackio-releases/therackio/big-data/applications/apache-kafka/confluent-kafka-bin-arm64/${CONFLUENT_KAFKA_VERSION}/kafka_${SCALA_VERSION}-${CONFLUENT_KAFKA_VERSION}.tar.gz && \
    tar -xzf ./kafka_${SCALA_VERSION}-${CONFLUENT_KAFKA_VERSION}.tar.gz && \
    rm -rf ./kafka_${SCALA_VERSION}-${CONFLUENT_KAFKA_VERSION}.tar.gz && \
    ln -s /opt/kafka_${SCALA_VERSION}-${CONFLUENT_KAFKA_VERSION} /opt/kafka && \
    cd ./kafka && \
    ls -al && \
    ls -al ./bin && \
    ls -al ./config

WORKDIR /opt/kafka
